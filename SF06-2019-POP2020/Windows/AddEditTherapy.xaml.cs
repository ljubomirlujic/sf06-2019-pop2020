﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditTherapy.xaml
    /// </summary>
    public partial class AddEditTherapy : Window
    {
        private EWindowStatus selectedStatus;
        private Therapy selectedTherapy;
        public AddEditTherapy(RegisteredUser user, Patient patient, Therapy therapy, EWindowStatus status = EWindowStatus.ADD)
        {
            InitializeComponent();


            this.DataContext = therapy;

            selectedTherapy = therapy;
            selectedStatus = status;
            
           
            if (status.Equals(EWindowStatus.EDIT) && therapy != null)
            {
                this.Title = "Edit therapy";
            }
            else
            {
                this.Title = "Add therapy";
            }
            if (user.UserType.Equals(EUserType.DOCTOR) && patient != null)
            {
                Doctor doctor = Util.Instance.findDoctorById(user.ID);
                List<Doctor> listaD = new List<Doctor>();
                listaD.Add(doctor);
                List<Patient> listaP = new List<Patient>();
                listaP.Add(patient);

                CmbDoctors.ItemsSource = listaD.Cast<Doctor>();
                CmbPatients.ItemsSource = listaP.Cast<Patient>();
            }
            else if (user.UserType.Equals(EUserType.ADMINISTRATOR))
            {
                CmbDoctors.ItemsSource = Util.Instance.Doctors.ToList().Cast<Doctor>();
                CmbPatients.ItemsSource = Util.Instance.Patients.ToList().Cast<Patient>();
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (selectedStatus.Equals(EWindowStatus.ADD))
            {
                selectedTherapy.Active = true;
                Util.Instance.Therapies.Add(selectedTherapy);
                Util.Instance.SaveEntity(selectedTherapy);
            }
            else if (selectedStatus.Equals(EWindowStatus.EDIT))
            {
                Util.Instance.UpdateEntity(selectedTherapy);
            }

            
            this.DialogResult = true;
            this.Close();
        }
    }
}
