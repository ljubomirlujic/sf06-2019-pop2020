﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllAppointments.xaml
    /// </summary>
    public partial class AllAppointments : Window
    {
        ICollectionView view;
        String selectedUsername;
        Patient selectedPatient;
        RegisteredUser currentUser;
        public AllAppointments(RegisteredUser user,String username = null, Patient patient = null)
        {
            InitializeComponent();
            currentUser = user;
            selectedUsername = username;
            selectedPatient = patient;
            UpdateView();

            view.Filter = CustomFilter;

            BTNReserved.Visibility = Visibility.Collapsed;
            if (currentUser.UserType.Equals(EUserType.PATIENT))
            {
                this.Title = "Patient: " + currentUser.Name;
                BTNAddAppointment.Visibility = Visibility.Collapsed;
                BTNEditAppointment.Visibility = Visibility.Collapsed;
                BTNDeleteAppointment.Visibility = Visibility.Collapsed;
            }
            if (patient == null && username == null)
            {
                this.Title = "AllAppointments";
            }
            if (currentUser.UserType.Equals(EUserType.DOCTOR))
            {
                this.Title = "Doctor: " + currentUser.Name;
                BTNEditAppointment.Visibility = Visibility.Collapsed;
            }
            if (patient != null)
            {
                this.Title = "Patient: " + selectedPatient.User.Name;
            }
      /*      else if (currentUser.UserType.Equals(EUserType.PATIENT) && username != null)
            {
                this.Title = "Doctor: " + username;
                BTNAddAppointment.Visibility = Visibility.Collapsed;
                BTNEditAppointment.Visibility = Visibility.Collapsed;
                BTNDeleteAppointment.Visibility = Visibility.Collapsed;
                BTNReserved.Visibility = Visibility.Visible;
            }

            else
            {
                this.Title = "Doctor: " + username;
            }*/

        }

        private bool CustomFilter(object obj)
        {
            Appointment appointment = obj as Appointment;
            // RegisteredUser user1 = (RegisteredUser)obj;
                if (appointment.Active)
                {
                    if (TxtSearch.Text != "")
                    {
                        return appointment.ID.Contains(TxtSearch.Text);
                    }
                    else
                        return true;
                }

            return false;
        }

        private void UpdateView()
        {
            if (currentUser.UserType.Equals(EUserType.DOCTOR))
            {
                Doctor doctor = Util.Instance.findDoctorById(currentUser.ID);
                view = CollectionViewSource.GetDefaultView(Util.Instance.findAppointmentByDoctor(doctor));
                DGAppointments.ItemsSource = view; // Util.Instance.Users;
                DGAppointments.IsSynchronizedWithCurrentItem = true;
                DGAppointments.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else if (currentUser.UserType.Equals(EUserType.PATIENT))
            {
                Patient patient = Util.Instance.findPatientById(currentUser.ID);
                view = CollectionViewSource.GetDefaultView(Util.Instance.findAppointmentByPatient(patient));
                DGAppointments.ItemsSource = view; // Util.Instance.Users;
                DGAppointments.IsSynchronizedWithCurrentItem = true;
                DGAppointments.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else if (selectedPatient != null)
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.findAppointmentsByUser(null,selectedPatient));
                DGAppointments.ItemsSource = view; // Util.Instance.Users;
                DGAppointments.IsSynchronizedWithCurrentItem = true;
                DGAppointments.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else if(selectedUsername != null)
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.findAppointmentsByUser(selectedUsername));
                DGAppointments.ItemsSource = view; // Util.Instance.Users;
                DGAppointments.IsSynchronizedWithCurrentItem = true;
                DGAppointments.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.Appointments);
                DGAppointments.ItemsSource = view; // Util.Instance.Users;
                DGAppointments.IsSynchronizedWithCurrentItem = true;
                DGAppointments.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
        }

        private void DGAppointment_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Patient") && selectedPatient !=null)
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void BTNAddAppointment_Click(object sender, RoutedEventArgs e)
        {
            Appointment newAppointment = new Appointment();

            if(selectedUsername != null)
            {
                Doctor doctor = Util.Instance.findDoctor(selectedUsername);
                AddEditAppointment add = new AddEditAppointment(currentUser,newAppointment,doctor,null);
                this.Hide();
                if (!(bool)add.ShowDialog())
                {

                }
                view.Refresh();
               
            }
            else if (selectedPatient != null)
            {
                AddEditAppointment add = new AddEditAppointment(currentUser, newAppointment,null,selectedPatient);
                this.Hide();
                if (!(bool)add.ShowDialog())
                {

                }
                view.Refresh();
            }
            else
            {
                AddEditAppointment add = new AddEditAppointment(currentUser, newAppointment);
                this.Hide();
                if (!(bool)add.ShowDialog())
                {

                }
                this.Show();
                view.Refresh();
            }

          
        }

        private void BTNEditAppointment_Click(object sender, RoutedEventArgs e)
        {
            if (DGAppointments.SelectedIndex != -1)
            {
                Appointment selected = view.CurrentItem as Appointment;
                Appointment oldAppointment = selected.Clone();

                AddEditAppointment add = new AddEditAppointment(currentUser, selected,null,null, EWindowStatus.EDIT);

                this.Hide();
                if (!(bool)add.ShowDialog())
                {
                    int index = Util.Instance.Appointments.ToList().FindIndex(a => a.ID.Equals(selected.ID));
                    Util.Instance.Appointments[index] = oldAppointment;
                }
                this.Show();
                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
        }


        private void BTNDeleteAppointment_Click(object sender, RoutedEventArgs e)
        {
            if (DGAppointments.SelectedIndex != -1)
            {
                Appointment selected = view.CurrentItem as Appointment;
                if (currentUser.UserType.Equals(EUserType.DOCTOR))
                {
                    if (selected.Status == EStatus.FREE)
                    {
                        selected.Active = false;
                        Util.Instance.DeleteEntity(selected);
                    }
                    else
                    {
                        MessageBox.Show("Appointment isn't free");
                    }
                }
                else
                {
                    selected.Active = false;
                    Util.Instance.DeleteEntity(selected);

                }
               

                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (selectedUsername == null && selectedPatient == null)
            {
                HomeWindow homeWindow = new HomeWindow(currentUser);
                homeWindow.Show();
            }
        }

        private void BTNReserved_Click(object sender, RoutedEventArgs e)
        {
            Appointment selected = view.CurrentItem as Appointment;

            if (selected.Status.Equals(EStatus.FREE))
            {
                selected.Patient = Util.Instance.findPatientById(currentUser.ID);
                selected.Status = EStatus.RESERVED;
                Util.Instance.UpdateEntity(selected);
            }
            view.Refresh();
            
        }
    }

}
