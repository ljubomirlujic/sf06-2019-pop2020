﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for ShowHospitalWindow.xaml
    /// </summary>
    public partial class ShowHospitalWindow : Window
    {
        public ShowHospitalWindow(Doctor doctor)
        {
            InitializeComponent();

            this.DataContext = doctor;
            CmbHospital.ItemsSource = Util.Instance.Hospitals.ToList().Cast<Hospital>();

            
        }
    }
}
