﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using SF06_2019_POP2020.Models;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllHospitals.xaml
    /// </summary>
    public partial class AllHospitals : Window
    {
        ICollectionView view;
        RegisteredUser CurrentUser;
        public AllHospitals(RegisteredUser user)
        {
            InitializeComponent();
            CurrentUser = user;
            UpdateView();

            view.Filter = CustomFilter;

            CmbDoctor.ItemsSource = Util.Instance.Doctors.ToList().Cast<Doctor>();
            CmbAdresa.ItemsSource = Util.Instance.Adresses.Cast<Adress>();
            if (user == null || !user.UserType.Equals(EUserType.ADMINISTRATOR))
            {
                BTNAddHospital.Visibility = Visibility.Collapsed;
                BTNEditHospital.Visibility = Visibility.Collapsed;
                BTNDeleteHospital.Visibility = Visibility.Collapsed;
            }
        }

        private bool CustomFilter(object obj)
        {
            Hospital hospital = obj as Hospital;

            if (hospital.Active)
                if (TxtSearch.Text != "")
                {
                    return hospital.Institution.Contains(TxtSearch.Text);
                }
                else
                    return true;
            return false;
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Hospitals);
            DGHospital.ItemsSource = view; // Util.Instance.Users;
            DGHospital.IsSynchronizedWithCurrentItem = true;
            DGHospital.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void DGHospital_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Active") || e.PropertyName.Equals("ID"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void BTNAddHospital_Click(object sender, RoutedEventArgs e)
        {
            Hospital newHospital = new Hospital();

            AddEditHospital add = new AddEditHospital(newHospital);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }

        private void BTNEditHospital_Click(object sender, RoutedEventArgs e)
        {
            Hospital selected = view.CurrentItem as Hospital;
            Hospital oldHospital = selected.Clone();

            AddEditHospital add = new AddEditHospital(selected, EWindowStatus.EDIT);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {
                int index = Util.Instance.Hospitals.ToList().FindIndex(a => a.ID.Equals(selected.ID));
                Util.Instance.Hospitals[index] = oldHospital;
            }
            this.Show();
            view.Refresh();
        }


        private void BTNDeleteHospital_Click(object sender, RoutedEventArgs e)
        {
            Hospital selected = view.CurrentItem as Hospital;
            selected.Active = false;
            Util.Instance.DeleteEntity(selected);

            view.Refresh();
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            HomeWindow homeWindow = new HomeWindow(CurrentUser);
            homeWindow.Show();
        }

        private void CmbDoctor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Doctor doctor = (Doctor)CmbDoctor.SelectedItem;
            List<Hospital> hospitalsList = Util.Instance.SearchHospitalByDoctor(doctor).ToList();

            view = CollectionViewSource.GetDefaultView(hospitalsList);
            DGHospital.ItemsSource = view; // Util.Instance.Users;
            DGHospital.IsSynchronizedWithCurrentItem = true;
            DGHospital.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void CmbAdresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Adress adress = (Adress)CmbAdresa.SelectedItem;
            List<Hospital> hospitalsList = Util.Instance.SearchHospitalByAdress(adress).ToList();

            view = CollectionViewSource.GetDefaultView(hospitalsList);
            DGHospital.ItemsSource = view; // Util.Instance.Users;
            DGHospital.IsSynchronizedWithCurrentItem = true;
            DGHospital.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

    }
}
