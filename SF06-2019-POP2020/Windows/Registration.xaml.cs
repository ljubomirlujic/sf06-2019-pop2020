﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class Registration : Window
    {
        private RegisteredUser selectedPatient;
        public Registration(RegisteredUser patient)
        {
            InitializeComponent();

            this.DataContext = patient;

            selectedPatient = patient;

            CmbGender.ItemsSource = Enum.GetValues(typeof(EGender)).Cast<EGender>();
            CmbAdress.ItemsSource = Util.Instance.Adresses.ToList().Cast<Adress>();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {

            if (isValid())
            {

                selectedPatient.Active = true;
                selectedPatient.UserType = EUserType.PATIENT;
                Patient patient = new Patient();
                patient.User = selectedPatient;

                Util.Instance.Users.Add(selectedPatient);
                Util.Instance.Patients.Add(patient);

                int id = Util.Instance.SaveEntity(selectedPatient);
                patient.ID = id;
                Util.Instance.SaveEntity(patient);

                this.DialogResult = true;
                this.Close();
            }
        }
        private bool isValid()
        {
            if (this.Title == "Add patient")
            {
                return !Validation.GetHasError(TxtName) && !Validation.GetHasError(TxtSurname) && !Validation.GetHasError(TxtUsername) && !Validation.GetHasError(TxtPassword)
                 && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtJMBG);
            }
            else
            {
                return !Validation.GetHasError(TxtName) && !Validation.GetHasError(TxtSurname) && !Validation.GetHasError(TxtPassword)
                 && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtJMBG);
            }

        }
    }
}
