﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllAdress.xaml
    /// </summary>

    public partial class AllAdress : Window
    {
        ICollectionView view;
        RegisteredUser CurrentUser;
        public AllAdress(RegisteredUser user)
        {
            InitializeComponent();
            CurrentUser = user;
            UpdateView();

            view.Filter = CustomFilter;
            
        }

        private bool CustomFilter(object obj)
        {
            Adress adress = obj as Adress;

            if (adress.Active)
                if (TxtSearch.Text != "")
                {
                    return adress.Street.Contains(TxtSearch.Text);
                }
                else
                    return true;
            return false;
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Adresses);
            DGAdress.ItemsSource = view; // Util.Instance.Users;
            DGAdress.IsSynchronizedWithCurrentItem = true;
            DGAdress.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void DGAdress_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Active") || e.PropertyName.Equals("ID"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void BTNAddAdress_Click(object sender, RoutedEventArgs e)
        {
            Adress newAdress = new Adress();

            AddEditAdress add = new AddEditAdress(newAdress);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }

        private void BTNEditAdress_Click(object sender, RoutedEventArgs e)
        {
            Adress selected = view.CurrentItem as Adress;
            Adress oldAdress = selected.Clone();

            AddEditAdress add = new AddEditAdress(selected, EWindowStatus.EDIT);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {
                int index = Util.Instance.Adresses.ToList().FindIndex(a => a.ID.Equals(selected.ID));
                Util.Instance.Adresses[index] = oldAdress;
            }
            this.Show();
            view.Refresh();
        }


        private void BTNDeleteAdress_Click(object sender, RoutedEventArgs e)
        {
            Adress selected = view.CurrentItem as Adress;
            selected.Active = false;
            Util.Instance.DeleteEntity(selected);

            view.Refresh();
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            HomeWindow homeWindow = new HomeWindow(CurrentUser);
            homeWindow.Show();
        }
    }
}
