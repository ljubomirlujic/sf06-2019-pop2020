﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditHospital.xaml
    /// </summary>
    public partial class AddEditHospital : Window
    {
        private EWindowStatus selectedStatus;
        private Hospital selectedHospital;
        public AddEditHospital(Hospital hospital, EWindowStatus status = EWindowStatus.ADD)
        {
            InitializeComponent();

            this.DataContext = hospital;

            selectedHospital = hospital;
            selectedStatus = status;

            CmbAdress.ItemsSource = Util.Instance.Adresses.ToList();
            if (status.Equals(EWindowStatus.EDIT) && hospital != null)
            {
                this.Title = "Edit hospital";
            }
            else
            {
                this.Title = "Add hospital";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {


            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (selectedStatus.Equals(EWindowStatus.ADD))
            {
                selectedHospital.Active = true;
                Util.Instance.Hospitals.Add(selectedHospital);
                Util.Instance.SaveEntity(selectedHospital);
            }
            if (selectedStatus.Equals(EWindowStatus.EDIT))
            {
                Util.Instance.UpdateEntity(selectedHospital);
            }
          
            this.DialogResult = true;
            this.Close();
        }
    }
}
