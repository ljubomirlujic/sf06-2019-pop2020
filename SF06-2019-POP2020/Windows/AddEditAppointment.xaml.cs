﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditAppointment.xaml
    /// </summary>
    public partial class AddEditAppointment : Window
    {
        private EWindowStatus selectedStatus;
        private Appointment selectedAppointment;
        private Doctor selectedDoctor;
        private RegisteredUser currentUser;
        public AddEditAppointment(RegisteredUser user, Appointment appointment, Doctor doctor = null, Patient patient = null, EWindowStatus status = EWindowStatus.ADD)
        {
            InitializeComponent();


            this.DataContext = appointment;

            selectedDoctor = doctor;
            selectedAppointment = appointment;
            selectedStatus = status;
            currentUser = user;
            CmbDoctors.ItemsSource = Util.Instance.Doctors.ToList().Cast<Doctor>();
            CmbStatus.ItemsSource = Enum.GetValues(typeof(EStatus)).Cast<EStatus>();
            CmbPatients.ItemsSource = Util.Instance.Patients.ToList().Cast<Patient>();
            if (patient != null)
            {
                List<Patient> lista = new List<Patient>();
                lista.Add(patient);
                CmbPatients.ItemsSource = lista.Cast<Patient>(); 
            }
            if (doctor != null)
            {
                List<Doctor> lista = new List<Doctor>();
                lista.Add(doctor);
                CmbDoctors.ItemsSource = lista.Cast<Doctor>();
                CmbPatients.IsEnabled = false;
                
            }
            if (user.UserType.Equals(EUserType.DOCTOR)){
                List<Doctor> lista = new List<Doctor>();
                Doctor doctor1 = Util.Instance.findDoctorById(user.ID);
                lista.Add(doctor1);
                CmbDoctors.ItemsSource = lista.Cast<Doctor>();
                CmbPatients.IsEnabled = false;
            }


            if (status.Equals(EWindowStatus.EDIT) && appointment != null)
            {
                this.Title = "Edit appointment";
            }
            else
            {
                this.Title = "Add appointment";
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (selectedStatus.Equals(EWindowStatus.ADD) && selectedDoctor == null && !currentUser.UserType.Equals(EUserType.DOCTOR))
            {
                selectedAppointment.Active = true;
                Util.Instance.Appointments.Add(selectedAppointment);
                Util.Instance.SaveEntity(selectedAppointment);
            }
            else if (selectedStatus.Equals(EWindowStatus.EDIT))
            {
                Util.Instance.UpdateEntity(selectedAppointment);
            }
            else if (selectedStatus.Equals(EWindowStatus.ADD) && selectedDoctor != null || selectedStatus.Equals(EWindowStatus.ADD) && currentUser.UserType.Equals(EUserType.DOCTOR))
            {
                selectedAppointment.Active = true;
                Util.Instance.Appointments.Add(selectedAppointment);
                Util.Instance.addFreeAppointment(selectedAppointment);
            }
            this.DialogResult = true;
            this.Close();
        }

    }
}
