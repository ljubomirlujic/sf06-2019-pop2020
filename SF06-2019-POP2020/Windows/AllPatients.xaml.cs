﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using SF06_2019_POP2020.Models;
using System.Collections.ObjectModel;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllPatients.xaml
    /// </summary>
    public partial class AllPatients : Window
    {
        ICollectionView view;
        RegisteredUser CurrentUser;
        public AllPatients(RegisteredUser user)
        {
            InitializeComponent();
            CurrentUser = user;
            UpdateView();

            view.Filter = CustomFilter;
            if (user.UserType.Equals(EUserType.PATIENT))
            {
                this.Title = "Profile: " + user.Username;
                BTNAddPatient.Visibility = Visibility.Collapsed;
                BTNDeletePatient.Visibility = Visibility.Collapsed;
            }
            if (user.UserType.Equals(EUserType.DOCTOR))
            {

                BTNAddPatient.Visibility = Visibility.Collapsed;
                BTNDeletePatient.Visibility = Visibility.Collapsed;
                BTNEditPatient.Visibility = Visibility.Collapsed;
                MIAppointment.Visibility = Visibility.Collapsed;
            }

        }

        private bool CustomFilter(object obj)
        {
            RegisteredUser user = obj as RegisteredUser;
            // RegisteredUser user1 = (RegisteredUser)obj;

            if (user.UserType.Equals(EUserType.PATIENT) && user.Active)
                if (TxtSearch.Text != "")
                {
                    return user.Name.Contains(TxtSearch.Text);
                }
                else
                    return true;
            return false;
        }

        private void UpdateView()
        {
            if (CurrentUser.UserType.Equals(EUserType.PATIENT))
            {
                ObservableCollection<RegisteredUser> lista = new ObservableCollection<RegisteredUser>();
                lista.Add(CurrentUser);
                view = CollectionViewSource.GetDefaultView(lista);
                DGPatients.ItemsSource = view; // Util.Instance.Users;
                DGPatients.IsSynchronizedWithCurrentItem = true;
                DGPatients.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else if (CurrentUser.UserType.Equals(EUserType.DOCTOR))
            {
                Doctor doctor = Util.Instance.findDoctorById(CurrentUser.ID);
                view = CollectionViewSource.GetDefaultView(Util.Instance.findPatientsByDoctor(doctor));
                DGPatients.ItemsSource = view; // Util.Instance.Users;
                DGPatients.IsSynchronizedWithCurrentItem = true;
                DGPatients.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.Users);
                DGPatients.ItemsSource = view; // Util.Instance.Users;
                DGPatients.IsSynchronizedWithCurrentItem = true;
                DGPatients.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
        }

        private void DGPatients_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID"))
                e.Column.Visibility = Visibility.Collapsed;
            if (CurrentUser.UserType.Equals(EUserType.PATIENT) && e.PropertyName.Equals("UserType"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            if (CurrentUser.UserType.Equals(EUserType.DOCTOR))
            {

                if (e.PropertyName.Equals("JMBG") || e.PropertyName.Equals("Username")
                || e.PropertyName.Equals("Password") || e.PropertyName.Equals("UserType"))
                {
                    e.Column.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void BTNAddPatient_Click(object sender, RoutedEventArgs e)
        {
            RegisteredUser newUser = new RegisteredUser();

            AddEditPatient add = new AddEditPatient(CurrentUser,newUser);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }

        private void BTNEditPatient_Click(object sender, RoutedEventArgs e)
        {
            if(DGPatients.SelectedIndex != -1)
            {
                RegisteredUser selected = view.CurrentItem as RegisteredUser;
                RegisteredUser oldPatient = selected.Clone();

                AddEditPatient add = new AddEditPatient(CurrentUser,selected, EWindowStatus.EDIT);

                this.Hide();
                if (!(bool)add.ShowDialog())
                {
                    int index = Util.Instance.Users.ToList().FindIndex(u => u.Username.Equals(selected.Username));
                    Util.Instance.Users[index] = oldPatient;
                }
                this.Show();
                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
           
        }

        private void BTNDeletePatient_Click(object sender, RoutedEventArgs e)
        {
            if (DGPatients.SelectedIndex != -1)
            {
                RegisteredUser selected = view.CurrentItem as RegisteredUser;
                selected.Active = false;
                Util.Instance.DeleteEntity(selected);

                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            HomeWindow homeWindow = new HomeWindow(CurrentUser);
            homeWindow.Show();
        }
        private void MIAppointment_Click(object sender, RoutedEventArgs e)
        {
            RegisteredUser selected = view.CurrentItem as RegisteredUser;

            Patient patient = Util.Instance.foundPatient(selected.Username);

            AllAppointments appointmentsWindow = new AllAppointments(CurrentUser,null, patient);

            this.Hide();
            if (!(bool)appointmentsWindow.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();

        }

        private void MITherapy_Click(object sender, RoutedEventArgs e)
        {
            RegisteredUser selected = view.CurrentItem as RegisteredUser;

            Patient patient = Util.Instance.foundPatient(selected.Username);

            AllTherapy therapyWindow = new AllTherapy(CurrentUser,patient);

            this.Hide();
            if (!(bool)therapyWindow.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();

        }
    }
}
