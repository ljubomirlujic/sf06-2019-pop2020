﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF06_2019_POP2020.Models;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditDoctor.xaml
    /// </summary>
    public partial class AddEditDoctor : Window
    {

        private EWindowStatus selectedStatus;
        private RegisteredUser selectedDoctor;
        private Doctor selectedDoc = new Doctor();
        public AddEditDoctor(RegisteredUser user ,RegisteredUser doctor, EWindowStatus status = EWindowStatus.ADD)
        {
            InitializeComponent();

            this.DataContext = doctor;

            selectedDoctor = doctor;
            selectedStatus = status;

            CmbUserType.ItemsSource = Enum.GetValues(typeof(EUserType)).Cast<EUserType>();
            CmbGender.ItemsSource = Enum.GetValues(typeof(EGender)).Cast<EGender>();
            CmbAdress.ItemsSource = Util.Instance.Adresses.Cast<Adress>();
            CmbHospital.ItemsSource = Util.Instance.Hospitals.ToList().Cast<Hospital>();
            CmbHospital.DataContext = selectedDoc;

            if (status.Equals(EWindowStatus.EDIT) && doctor != null)
            {
                this.Title = "Edit doctor";
                TxtUsername.IsEnabled = false;
            }
            else
            {
                this.Title = "Add doctor";
            }
            if (user.UserType.Equals(EUserType.DOCTOR))
            {
                CmbHospital.IsEnabled = false;
                CmbUserType.IsEnabled = false;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {

            if (isValid())
            {
                if (CmbAdress.SelectedIndex != -1)
                {
                    if (selectedStatus.Equals(EWindowStatus.ADD))
                    {
                        selectedDoctor.Active = true;
                        
                        

                        Util.Instance.Users.Add(selectedDoctor);
                        Util.Instance.Doctors.Add(selectedDoc);

                        int id = Util.Instance.SaveEntity(selectedDoctor);
                        selectedDoc.ID = id;
                        Util.Instance.SaveEntity(selectedDoc);
                    }

                    Util.Instance.UpdateEntity(selectedDoctor);
                    this.DialogResult = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You must select Adress!");

                }
            }

           
        }

        private bool isValid()
        {
            if (this.Title == "Add doctor")
            {
                return !Validation.GetHasError(TxtName) && !Validation.GetHasError(TxtSurname) && !Validation.GetHasError(TxtUsername) && !Validation.GetHasError(TxtPassword)
                 && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtJMBG);
            }
            else
            {
                return !Validation.GetHasError(TxtName) && !Validation.GetHasError(TxtSurname) && !Validation.GetHasError(TxtPassword)
                 && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtJMBG);
            }

        }
    }
}
