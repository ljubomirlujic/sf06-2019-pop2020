﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllTherapy.xaml
    /// </summary>
    public partial class AllTherapy : Window
    {
        ICollectionView view;
        Patient selectedPatient;
        RegisteredUser currentUser;
        public AllTherapy(RegisteredUser user, Patient patient = null)
        {
            InitializeComponent();
            currentUser = user;
            selectedPatient = patient;
            UpdateView();

            view.Filter = CustomFilter;

            if (patient == null)
            {
                this.Title = "AllTherapy";
            }
            else
            {
                this.Title = "Patient: " + selectedPatient.User.Name;

            }

            if (currentUser.UserType.Equals(EUserType.PATIENT))
            {
                BTNAddTherapy.Visibility = Visibility.Collapsed;
                BTNEditTherapy.Visibility = Visibility.Collapsed;
                BTNDeleteTherapy.Visibility = Visibility.Collapsed;
            }
            if (currentUser.UserType.Equals(EUserType.DOCTOR))
            {
                BTNEditTherapy.Visibility = Visibility.Collapsed;
                BTNDeleteTherapy.Visibility = Visibility.Collapsed;
            }

        }

        private bool CustomFilter(object obj)
        {
            Therapy therapy = obj as Therapy;
            // RegisteredUser user1 = (RegisteredUser)obj;

                if (therapy.Active)
                {
                    if (TxtSearch.Text != "")
                    {
                        return therapy.Description.Contains(TxtSearch.Text);
                    }
                    
                    else
                        return true;
                }
        
            return false;
        }

        private void UpdateView()
        {
            //DGLekari.ItemsSource = null;
            if (currentUser.UserType.Equals(EUserType.PATIENT))
            {
                Patient patient = Util.Instance.findPatientById(currentUser.ID);
                view = CollectionViewSource.GetDefaultView(Util.Instance.findTherapByPatient(patient));
                DGTherapy.ItemsSource = view; // Util.Instance.Users;
                DGTherapy.IsSynchronizedWithCurrentItem = true;
                DGTherapy.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else if(currentUser.UserType.Equals(EUserType.PATIENT) && selectedPatient != null)
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.findTherapiesByUser(selectedPatient));
                DGTherapy.ItemsSource = view; // Util.Instance.Users;
                DGTherapy.IsSynchronizedWithCurrentItem = true;
                DGTherapy.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else if (selectedPatient == null && currentUser.UserType.Equals(EUserType.ADMINISTRATOR))
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.Therapies);
                DGTherapy.ItemsSource = view; // Util.Instance.Users;
                DGTherapy.IsSynchronizedWithCurrentItem = true;
                DGTherapy.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }

            else
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.findTherapiesByUser(selectedPatient));
                DGTherapy.ItemsSource = view; // Util.Instance.Users;
                DGTherapy.IsSynchronizedWithCurrentItem = true;
                DGTherapy.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
        }

        private void DGTherapy_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID") || e.PropertyName.Equals("Patient") && selectedPatient != null)
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void BTNAddTherapy_Click(object sender, RoutedEventArgs e)
        {
            Therapy newTherapy = new Therapy();

            AddEditTherapy add = new AddEditTherapy(currentUser, selectedPatient, newTherapy);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }

        private void BTNEditTherapy_Click(object sender, RoutedEventArgs e)
        {
            if (DGTherapy.SelectedIndex != -1)
            {
                Therapy selected = view.CurrentItem as Therapy;
                Therapy oldTherapy = selected.Clone();

                AddEditTherapy add = new AddEditTherapy(currentUser,null,selected, EWindowStatus.EDIT);

                this.Hide();
                if (!(bool)add.ShowDialog())
                {
                    int index = Util.Instance.Therapies.ToList().FindIndex(t => t.ID.Equals(selected.ID));
                    Util.Instance.Therapies[index] = oldTherapy;
                }
                this.Show();
                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
        }


        private void BTNDeleteTherapy_Click(object sender, RoutedEventArgs e)
        {
            if (DGTherapy.SelectedIndex != -1)
            {
                Therapy selected = view.CurrentItem as Therapy;
                selected.Active = false;
                Util.Instance.DeleteEntity(selected);

                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (selectedPatient == null)
            {
                HomeWindow homeWindow = new HomeWindow(currentUser);
                homeWindow.Show();
            }
        }
    }
}
