﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditPatient.xaml
    /// </summary>
    public partial class AddEditPatient : Window
    {

        private EWindowStatus selectedStatus;
        private RegisteredUser selectedPatient;
        public AddEditPatient(RegisteredUser user, RegisteredUser patient, EWindowStatus status = EWindowStatus.ADD)
        {
            InitializeComponent();

            this.DataContext = patient;

            selectedPatient = patient;
            selectedStatus = status;

            CmbUserType.ItemsSource = Enum.GetValues(typeof(EUserType)).Cast<EUserType>();
            CmbGender.ItemsSource = Enum.GetValues(typeof(EGender)).Cast<EGender>();
            CmbAdress.ItemsSource = Util.Instance.Adresses.ToList().Cast<Adress>();

            if (status.Equals(EWindowStatus.EDIT) && patient != null)
            {
                this.Title = "Edit patient";
                TxtUsername.IsEnabled = false;
                TxtUsername.Foreground = Brushes.Black;
            }
            else
            {
                this.Title = "Add patient";
            }
            if (user.UserType.Equals(EUserType.PATIENT))
            {
                CmbUserType.IsEnabled = false;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {

            if (isValid())
            {
                if (CmbAdress.SelectedIndex != -1)
                {

                    if (selectedStatus.Equals(EWindowStatus.ADD))
                    {
                        selectedPatient.Active = true;
                        Patient patient = new Patient();
                        patient.User = selectedPatient;

                        Util.Instance.Users.Add(selectedPatient);
                        Util.Instance.Patients.Add(patient);

                        int id = Util.Instance.SaveEntity(selectedPatient);
                        patient.ID = id;
                        Util.Instance.SaveEntity(patient);
                    }
                    else if (selectedStatus.Equals(EWindowStatus.EDIT))
                    {
                        Util.Instance.UpdateEntity(selectedPatient);
                    }

                    
                    this.DialogResult = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("You must select Adress!");

                }
            }
        }
        private bool isValid()
        {
            if(this.Title == "Add patient")
            {
                return !Validation.GetHasError(TxtName) && !Validation.GetHasError(TxtSurname) && !Validation.GetHasError(TxtUsername) && !Validation.GetHasError(TxtPassword)
                 && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtJMBG);
            }
            else
            {
                return !Validation.GetHasError(TxtName) && !Validation.GetHasError(TxtSurname) && !Validation.GetHasError(TxtPassword)
                 && !Validation.GetHasError(TxtEmail) && !Validation.GetHasError(TxtJMBG);
            }
            

        }
    }
}

