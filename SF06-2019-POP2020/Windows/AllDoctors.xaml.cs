﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF06_2019_POP2020.Models;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllDoctors.xaml
    /// </summary>
    public partial class AllDoctors : Window
    {
        ICollectionView view;
        RegisteredUser CurrentUser;
        public AllDoctors(RegisteredUser user)
        {
            InitializeComponent();
            CurrentUser = user;
            UpdateView();

            view.Filter = CustomFilter;

            if (user.UserType.Equals(EUserType.PATIENT))
            {
                BTNAddDoctor.Visibility = Visibility.Collapsed;
                BTNEditDoctor.Visibility = Visibility.Collapsed;
                BTNDeletDoctor.Visibility = Visibility.Collapsed;
                MIHospital.Visibility = Visibility.Collapsed;
            }
            if (user.UserType.Equals(EUserType.DOCTOR))
            {
                this.Title = "Profile: " + user.Username;
                BTNAddDoctor.Visibility = Visibility.Collapsed;
                BTNDeletDoctor.Visibility = Visibility.Collapsed;
                MIHospital.Visibility = Visibility.Collapsed;
            }
        }

        private bool CustomFilter(object obj)
        {
            RegisteredUser user = obj as RegisteredUser;
            // RegisteredUser user1 = (RegisteredUser)obj;

            if (user.UserType.Equals(EUserType.DOCTOR) && user.Active)
                if (TxtSearch.Text != "")
                {
                    return user.Name.Contains(TxtSearch.Text);
                }
                else
                    return true;
            return false;
        }

        private void UpdateView()
        {
            if (CurrentUser.UserType.Equals(EUserType.DOCTOR))
            {
                ObservableCollection<RegisteredUser> lista = new ObservableCollection<RegisteredUser>();
                lista.Add(CurrentUser);
                view = CollectionViewSource.GetDefaultView(lista);
                DGDoctors.ItemsSource = view; // Util.Instance.Users;
                DGDoctors.IsSynchronizedWithCurrentItem = true;
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.Users);
                DGDoctors.ItemsSource = view; // Util.Instance.Users;
                DGDoctors.IsSynchronizedWithCurrentItem = true;
            }
        }

        private void DGDoctors_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error") || e.PropertyName.Equals("ID"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            } 
            if (CurrentUser.UserType.Equals(EUserType.PATIENT)){

                if(e.PropertyName.Equals("JMBG") || e.PropertyName.Equals("Username")
                || e.PropertyName.Equals("Password") || e.PropertyName.Equals("UserType"))
                {
                    e.Column.Visibility = Visibility.Collapsed;
                }
            }
                 
            
        }

        private void BTNAddDoctor_Click(object sender, RoutedEventArgs e)
        {
            RegisteredUser newUser = new RegisteredUser();

            AddEditDoctor add = new AddEditDoctor(CurrentUser,newUser);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }

        private void BTNEditDoctor_Click(object sender, RoutedEventArgs e)
        {
            if (DGDoctors.SelectedIndex != -1)
            {
                RegisteredUser selected = view.CurrentItem as RegisteredUser;
                RegisteredUser oldDoctor = selected.Clone();

                AddEditDoctor add = new AddEditDoctor(CurrentUser,selected, EWindowStatus.EDIT);

                this.Hide();
                if (!(bool)add.ShowDialog())
                {
                    int index = Util.Instance.Users.ToList().FindIndex(u => u.Username.Equals(selected.Username));
                    Util.Instance.Users[index] = oldDoctor;
                }
                this.Show();
                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
        }


        private void BTNDeleteDoctor_Click(object sender, RoutedEventArgs e)
        {
            if (DGDoctors.SelectedIndex != -1)
            {
                RegisteredUser selected = view.CurrentItem as RegisteredUser;
                selected.Active = false;
                Util.Instance.DeleteEntity(selected);

                view.Refresh();
            }
            else
            {
                MessageBox.Show("You must select a row");
            }
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            HomeWindow homeWindow = new HomeWindow(CurrentUser);
            homeWindow.Show();
        }

        private void MIAppointment_Click(object sender, RoutedEventArgs e)
        {
            RegisteredUser selected = view.CurrentItem as RegisteredUser;

            if(CurrentUser != null)
            {
                AllAppointments appointment = new AllAppointments(CurrentUser, selected.Username);
                this.Hide();
                if (!(bool)appointment.ShowDialog())
                {

                }
                this.Show();
                view.Refresh();
            }
            else
            {
                AllAppointments appointment = new AllAppointments(null, selected.Username);
                this.Hide();
                if (!(bool)appointment.ShowDialog())
                {

                }
                this.Show();
                view.Refresh();
            }
           
        }

        private void MIHospital_Click(object sender, RoutedEventArgs e)
        {
            RegisteredUser selected = view.CurrentItem as RegisteredUser;
            Doctor doctor = Util.Instance.findDoctorById(selected.ID);
            ShowHospitalWindow shw = new ShowHospitalWindow(doctor);
            this.Hide();
            if (!(bool)shw.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }
    }
}

