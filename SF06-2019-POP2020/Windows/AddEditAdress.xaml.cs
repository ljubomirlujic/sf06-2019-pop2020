﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditAdress.xaml
    /// </summary>
    public partial class AddEditAdress : Window
    {
        private EWindowStatus selectedStatus;
        private Adress selectedAdress;
        public AddEditAdress(Adress adress, EWindowStatus status = EWindowStatus.ADD)
        {
            InitializeComponent();

            this.DataContext = adress;

            selectedAdress = adress;
            selectedStatus = status;


            if (status.Equals(EWindowStatus.EDIT) && adress != null)
            {
                this.Title = "Edit adress";
            }
            else
            {
                this.Title = "Add adress";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            if (selectedStatus.Equals(EWindowStatus.ADD))
            {
                selectedAdress.Active = true;
                Util.Instance.Adresses.Add(selectedAdress);
                Util.Instance.SaveEntity(selectedAdress);
            }
            if (selectedStatus.Equals(EWindowStatus.EDIT))
            {
                Util.Instance.UpdateEntity(selectedAdress);
            }
           
            this.DialogResult = true;
            this.Close();
        }
    }
}
