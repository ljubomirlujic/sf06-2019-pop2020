﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF06_2019_POP2020
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            Util.Instance.Initialize();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RegisteredUser user = findUser();
            if(user != null && user.Active == true)
            {
                HomeWindow hw = new HomeWindow(user);
                this.Close();
                hw.Show();
            }
            else
            {
                MessageBox.Show("Wrong login info!");
            }
                
            
        }
        private RegisteredUser findUser()
        {
            string username = TxtUsername.Text;
            string password = PBPassword.Password;

            RegisteredUser user;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users u join adress a on u.AdressId = a.id
                                        where u.username = @username and u.password = @password";
                command.Parameters.Add(new SqlParameter("username", username));
                command.Parameters.Add(new SqlParameter("password", password));

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Enum.TryParse(reader.GetString(7), out EGender gender);
                    Enum.TryParse(reader.GetString(8), out EUserType userType);
                    user = new RegisteredUser
                    {
                        ID = reader.GetInt32(0),
                        Username = reader.GetString(1),
                        Password = reader.GetString(2),
                        Name = reader.GetString(3),
                        Surname = reader.GetString(4),
                        Email = reader.GetString(5),
                        JMBG = reader.GetString(6),
                        Gender = gender,
                        UserType = userType,
                        Active = reader.GetBoolean(10),
                        Adress = new Adress
                        {
                            City = reader.GetString(14),
                            Number = reader.GetString(13),
                            State = reader.GetString(15),
                            Street = reader.GetString(12),
                            ID = reader.GetInt32(11).ToString(),
                            Active = reader.GetBoolean(16)
                        }
                    };
                    return user;

                }

                reader.Close();
            }
            return null;

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            HomeWindow hw = new HomeWindow(null);
            this.Close();
            hw.Show();
        }
    }
}
