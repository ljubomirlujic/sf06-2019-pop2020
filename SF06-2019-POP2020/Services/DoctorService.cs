﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF06_2019_POP2020.Models;


namespace SF06_2019_POP2020.Services
{
    public class DoctorService : EntityService
    {
        public void deleteEntity(object obj)
        {
            throw new NotImplementedException();
        }

        public void readEntity()
        {
            Util.Instance.Doctors = new ObservableCollection<Doctor>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from doctor";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    Util.Instance.Doctors.Add(new Doctor
                    {
                        ID = id,
                        Hospital = Util.Instance.findHospitalById(reader.GetInt32(1))[0],
                        User = Util.Instance.foundUserById(id)
                    });

                }

                reader.Close();
            }
        }

        public int saveEntity(object obj)
        {
            Doctor doctor = obj as Doctor;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Doctor (Id, HospitalId)
                                        output inserted.id VALUES (@Id, @HospitalId)";

                command.Parameters.Add(new SqlParameter("Id", doctor.ID));
                command.Parameters.Add(new SqlParameter("HospitalId", doctor.Hospital.ID));

                return (int)command.ExecuteScalar();
            }
        }

        public void updateEntity(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
