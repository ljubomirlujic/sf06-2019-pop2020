﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Services
{
    public class TherapyService : EntityService
    {
        public void deleteEntity(object obj)
        {
            throw new NotImplementedException();
        }

        public void readEntity()
        {
            Util.Instance.Therapies = new ObservableCollection<Therapy>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from Therapy";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    Util.Instance.Therapies.Add(new Therapy
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Description = reader.GetString(1),
                        Doctor = Util.Instance.findDoctorById(reader.GetInt32(2)),
                        Patient = Util.Instance.findPatientById(reader.GetInt32(3)),
                        Active = reader.GetBoolean(4)
                    });

                }

                reader.Close();
            }
        }

        public int saveEntity(object obj)
        {
            Therapy therapy = obj as Therapy;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Therapy (Description, DoctorId, PatientId, Active)
                                        output inserted.id VALUES (@Description, @DoctorId, @PatientId, @Active)";
                command.Parameters.Add(new SqlParameter("Description", therapy.Description));
                command.Parameters.Add(new SqlParameter("DoctorId", therapy.Doctor.ID));
                command.Parameters.Add(new SqlParameter("PatientId", therapy.Patient.ID));
                command.Parameters.Add(new SqlParameter("Active", therapy.Active));

                return (int)command.ExecuteScalar();
            }
        }

        public void updateEntity(object obj)
        {
            Therapy therapy = obj as Therapy;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Therapy SET Description = @Description, DoctorId = @DoctorId,
                                        PatientId = @PatientId where id = @id";

                command.Parameters.Add(new SqlParameter("id", therapy.ID));
                command.Parameters.Add(new SqlParameter("Description", therapy.Description));
                command.Parameters.Add(new SqlParameter("DoctorId", therapy.Doctor.ID));
                command.Parameters.Add(new SqlParameter("PatientId", therapy.Patient.ID));


                command.ExecuteScalar();
            }
        }
    }
}
