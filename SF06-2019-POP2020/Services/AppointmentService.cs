﻿using SF06_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Services
{
    public class AppointmentService : EntityService
    {
        public void deleteEntity(object obj)
        {
            Appointment appointment = obj as Appointment;
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update dbo.Appointment 
                                        SET Active = @Active 
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", appointment.ID));
                command.Parameters.Add(new SqlParameter("Active", appointment.Active));


                command.ExecuteNonQuery();
            }
        }

        public void readEntity()
        {
            Util.Instance.Appointments = new ObservableCollection<Appointment>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from appointment a left join Doctor d on a.DoctorId = d.id left join patient p  on a.PatientId = p.id";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Patient patient;
                    if (reader.IsDBNull(2))
                    {
                        patient = null;   
                    }
                    else
                    {
                        patient = Util.Instance.findPatientById(reader.GetInt32(2));
                    }
                    Enum.TryParse(reader.GetString(4), out EStatus estatus);
                    Util.Instance.Appointments.Add(new Appointment
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Doctor = Util.Instance.findDoctorById(reader.GetInt32(1)),
                        Patient = patient,
                        Date = reader.GetDateTime(3),
                        Status = estatus,
                        Active = reader.GetBoolean(5)
                    });

                }

                reader.Close();
            }
        }

        public int saveEntity(object obj)
        {
            Appointment appointment = obj as Appointment;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Appointment (DoctorId, PatientId, Date, Status, Active)
                                        output inserted.id VALUES (@DoctorId, @PatientId, @Date, @Status, @Active)";

                command.Parameters.Add(new SqlParameter("DoctorId", appointment.Doctor.ID));
                command.Parameters.Add(new SqlParameter("PatientId", appointment.Patient.ID));
                command.Parameters.Add(new SqlParameter("Date", appointment.Date));
                command.Parameters.Add(new SqlParameter("Status", appointment.Status));
                command.Parameters.Add(new SqlParameter("Active", appointment.Active));

                return (int)command.ExecuteScalar();
            }
        }

        public void updateEntity(object obj)
        {
            Appointment appointment = obj as Appointment;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Appointment SET DoctorId = @DoctorId, PatientId = @PatientId,
                                        Date = @Date, Status = @Status where id = @id";

                command.Parameters.Add(new SqlParameter("id", appointment.ID));
                command.Parameters.Add(new SqlParameter("DoctorId", appointment.Doctor.ID));
                command.Parameters.Add(new SqlParameter("PatientId", appointment.Patient.ID));
                command.Parameters.Add(new SqlParameter("Date", appointment.Date));
                command.Parameters.Add(new SqlParameter("Status", appointment.Status));




                command.ExecuteScalar();
            }
        }

       
    }
}
