﻿using SF06_2019_POP2020.Models;
using SF06_2019_POP2020.MyExceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Services
{
    public class HospitalService : EntityService
    {
        public void readEntity()
        {
            Util.Instance.Hospitals = new ObservableCollection<Hospital>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from hospital h join adress a on h.AdressId = a.id ";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Util.Instance.Hospitals.Add(new Hospital
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Institution = reader.GetString(1),
                        Active = reader.GetBoolean(3),
                        Adress = new Adress
                        {
                            City = reader.GetString(7),
                            Number = reader.GetString(6),
                            State = reader.GetString(8),
                            Street = reader.GetString(5),
                            ID = reader.GetInt32(4).ToString(),
                            Active = reader.GetBoolean(9)
                        }
                    });

                }

                reader.Close();
            }
        }
        public int saveEntity(object obj)
        {
            Hospital hospital = obj as Hospital;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Hospital (Institution, AdressId, Active)
                                        output inserted.id VALUES (@Institution,@AdressId,@Active)";
                command.Parameters.Add(new SqlParameter("Institution", hospital.Institution));
                command.Parameters.Add(new SqlParameter("AdressId", hospital.Adress.ID));
                command.Parameters.Add(new SqlParameter("Active", hospital.Active));

                return (int)command.ExecuteScalar();
            }
        }
        public void deleteEntity(Object obj)
        {
            Hospital hospital = obj as Hospital;
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update dbo.Hospital 
                                        SET Active = @Active 
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", hospital.ID));
                command.Parameters.Add(new SqlParameter("Active", hospital.Active));


                command.ExecuteNonQuery();
            }
        }

        public void updateEntity(object obj)
        {
            Hospital hospital = obj as Hospital;
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update dbo.Hospital 
                                        SET Institution = @Institution,
                                        AdressId = @AdressId
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", hospital.ID));
                command.Parameters.Add(new SqlParameter("Institution", hospital.Institution));
                command.Parameters.Add(new SqlParameter("AdressId", hospital.Adress.ID));


                command.ExecuteNonQuery();
            }
        }
    }
        
}
