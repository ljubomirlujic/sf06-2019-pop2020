﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF06_2019_POP2020.Models;
using SF06_2019_POP2020.MyExceptions;

namespace SF06_2019_POP2020.Services
{
    public class RegistredUserService : EntityService
    {
        public void deleteEntity(object obj)
        {
            RegisteredUser user = obj as RegisteredUser;
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update dbo.Users 
                                        SET Active = @Active 
                                        where username = @Username";

                command.Parameters.Add(new SqlParameter("Active", user.Active));
                command.Parameters.Add(new SqlParameter("Username", user.Username));

                command.ExecuteNonQuery();
            }
        }

        public void readEntity()
        {
            Util.Instance.Users = new ObservableCollection<RegisteredUser>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users u join adress a on u.AdressId = a.id";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Enum.TryParse(reader.GetString(7), out EGender gender);
                    Enum.TryParse(reader.GetString(8), out EUserType userType);
                    Util.Instance.Users.Add(new RegisteredUser
                    {
                        ID = reader.GetInt32(0),
                        Username = reader.GetString(1),
                        Password = reader.GetString(2),
                        Name = reader.GetString(3),
                        Surname = reader.GetString(4),
                        Email = reader.GetString(5),
                        JMBG = reader.GetString(6),
                        Gender = gender,
                        UserType = userType,
                        Active = reader.GetBoolean(10),
                        Adress = new Adress
                        {
                            City = reader.GetString(14),
                            Number = reader.GetString(13),
                            State = reader.GetString(15),
                            Street = reader.GetString(12),
                            ID = reader.GetInt32(11).ToString(),
                            Active = reader.GetBoolean(16)
                        }
                    });

                }

                reader.Close();
            }



        }
        public int saveEntity(object obj)
        {

            RegisteredUser user = obj as RegisteredUser;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Users (Username, Password, Name, Surname, Email, JMBG,
                                        Gender, UserType, AdressId, Active)
                                        output inserted.id VALUES (@Username, @Password, @Name, @Surname, @Email, @JMBG, @Gender,
                                        @UserType, @AdressId, @Active)";

                command.Parameters.Add(new SqlParameter("Username", user.Username));
                command.Parameters.Add(new SqlParameter("Password", user.Password));
                command.Parameters.Add(new SqlParameter("Name", user.Name));
                command.Parameters.Add(new SqlParameter("Surname", user.Surname));
                command.Parameters.Add(new SqlParameter("UserType", user.UserType.ToString()));
                command.Parameters.Add(new SqlParameter("Gender", user.Gender.ToString()));
                command.Parameters.Add(new SqlParameter("Email", user.Email));
                command.Parameters.Add(new SqlParameter("JMBG", user.JMBG));
                command.Parameters.Add(new SqlParameter("AdressId", user.Adress.ID));
                command.Parameters.Add(new SqlParameter("Active", user.Active));

                return (int)command.ExecuteScalar();
            }

        }
        public void updateEntity(object obj)
        {
            RegisteredUser user = obj as RegisteredUser;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE dbo.Users SET Username = @Username, Password = @Password, Name = @Name,
                                        Surname = @Surname, Email = @Email, JMBG = @JMBG, Gender = @Gender,
                                        UserType = @UserType, AdressId = @AdressId where id = @id";

                command.Parameters.Add(new SqlParameter("id", user.ID));
                command.Parameters.Add(new SqlParameter("Username", user.Username));
                command.Parameters.Add(new SqlParameter("Password", user.Password));
                command.Parameters.Add(new SqlParameter("Name", user.Name));
                command.Parameters.Add(new SqlParameter("Surname", user.Surname));
                command.Parameters.Add(new SqlParameter("UserType", user.UserType.ToString()));
                command.Parameters.Add(new SqlParameter("Gender", user.Gender.ToString()));
                command.Parameters.Add(new SqlParameter("Email", user.Email));
                command.Parameters.Add(new SqlParameter("JMBG", user.JMBG));
                command.Parameters.Add(new SqlParameter("AdressId", user.Adress.ID));

                command.ExecuteScalar();
            }
        }


    }
}
