﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF06_2019_POP2020.Models;

namespace SF06_2019_POP2020.Services
{
    public class PatientService : EntityService
    {
        public void deleteEntity(object obj)
        {
            throw new NotImplementedException();
        }

        public void readEntity()
        {
            Util.Instance.Patients = new ObservableCollection<Patient>();
            

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from Patient";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    Util.Instance.Patients.Add(new Patient
                    {
                        ID = id,
                        User = Util.Instance.foundUserById(id)
                    });
                }

                reader.Close();
            }
        }

        public int saveEntity(object obj)
        {
            Patient patient = obj as Patient;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Patient (Id)
                                        output inserted.id VALUES (@Id)";

                command.Parameters.Add(new SqlParameter("Id", patient.ID));

                return (int)command.ExecuteScalar();
            }
        }

        public void updateEntity(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
