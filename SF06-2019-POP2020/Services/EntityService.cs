﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Services
{
    public interface EntityService
    {
        void readEntity();
        int saveEntity(object obj);
        void deleteEntity(object obj);
        void updateEntity(object obj);
    }
}
