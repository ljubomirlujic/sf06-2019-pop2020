﻿using SF06_2019_POP2020.Models;
using SF06_2019_POP2020.MyExceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Services
{
    public class AdressService : EntityService
    {
        public void deleteEntity(object obj)
        {
            Adress adress = obj as Adress;
            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update dbo.Adress 
                                        SET Active = @Active 
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", adress.ID));
                command.Parameters.Add(new SqlParameter("Active", adress.Active));


                command.ExecuteNonQuery();
            }
        }

        public void readEntity()
        {
            Util.Instance.Adresses = new ObservableCollection<Adress>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from adress where Active = 1";

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Util.Instance.Adresses.Add(new Adress
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Street = reader.GetString(1),
                        Number = reader.GetString(2),
                        City = reader.GetString(3),
                        State = reader.GetString(4),
                        Active = reader.GetBoolean(5)
                    });

                }

                reader.Close();
            }
        }

        public int saveEntity(object obj)
        {
            Adress adress = obj as Adress;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Adress (Street, Number, City, State, Active)
                                        output inserted.id VALUES (@Street, @Number, @City, @State, @Active)";
                command.Parameters.Add(new SqlParameter("Street", adress.Street));
                command.Parameters.Add(new SqlParameter("Number", adress.Number));
                command.Parameters.Add(new SqlParameter("City", adress.City));
                command.Parameters.Add(new SqlParameter("State", adress.State));
                command.Parameters.Add(new SqlParameter("Active", adress.Active));

                return (int)command.ExecuteScalar();
            }
        }

        public void updateEntity(object obj)
        {
            Adress adress = obj as Adress;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update dbo.Adress SET Street = @Street,
                                        Number = @Number, City = @City, State = @State
                                        where id = @id";
                command.Parameters.Add(new SqlParameter("id", adress.ID));
                command.Parameters.Add(new SqlParameter("Street", adress.Street));
                command.Parameters.Add(new SqlParameter("Number", adress.Number));
                command.Parameters.Add(new SqlParameter("City", adress.City));
                command.Parameters.Add(new SqlParameter("State", adress.State));

                command.ExecuteScalar();
            }
        }
    }
}
