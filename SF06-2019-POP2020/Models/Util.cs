﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF06_2019_POP2020.Services;
using System.Collections.ObjectModel;
using SF06_2019_POP2020.Models;
using System.Data.SqlClient;

namespace SF06_2019_POP2020.Models
{
    public sealed class Util
    {
        public static String CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;
                                                   Integrated Security=True;Connect Timeout=30;Encrypt=False;
                                                   TrustServerCertificate=False;ApplicationIntent=ReadWrite;
                                                   MultiSubnetFailover=False";
        private static readonly Util instance = new Util();
        EntityService _userService;
        EntityService _doctorService;
        EntityService _patientService;
        EntityService _adressService;
        EntityService _hospitalService;
        EntityService _appointmentService;
        EntityService _therapyService;

        public ObservableCollection<RegisteredUser> Users { get; set; }
        public ObservableCollection<Doctor> Doctors { get; set; }
        public ObservableCollection<Patient> Patients { get; set; }
        public ObservableCollection<Adress> Adresses { get; set; }
        public ObservableCollection<Hospital> Hospitals { get; set; }
        public ObservableCollection<Appointment> Appointments { get; set; }
        public ObservableCollection<Therapy> Therapies { get; set; }

        private Util()
        {
            _userService = new RegistredUserService();
            _doctorService = new DoctorService();
            _patientService = new PatientService();
            _adressService = new AdressService();
            _hospitalService = new HospitalService();
            _appointmentService = new AppointmentService();
            _therapyService = new TherapyService();

        }
        static Util()
        {

        }

        public static Util Instance
        {
            get
            {
                return instance;
            }
        }

        public void Initialize()
        {
            Users = new ObservableCollection<RegisteredUser>();
            Doctors = new ObservableCollection<Doctor>();
            Patients = new ObservableCollection<Patient>();
            Adresses = new ObservableCollection<Adress>();
            Hospitals = new ObservableCollection<Hospital>();
            Appointments = new ObservableCollection<Appointment>();
            Therapies = new ObservableCollection<Therapy>();

           /* Adress adress1 = new Adress
            {
                City = "Novi Sad",
                Number = "6",
                State = "Srbija",
                Street = "Tolstojeva",
                ID = "1",
                Active = true
            };
            Adress adress2 = new Adress
            {
                City = "Novi Sad",
                Number = "17",
                State = "Srbija",
                Street = "Puskinova",
                ID = "2",
                Active = true
            };
            Adress adress3 = new Adress
            {
                City = "Novi Sad",
                Number = "23",
                State = "Srbija",
                Street = "Bulevar",
                ID = "3",
                Active = true
            };


            Adresses.Add(adress1);
            Adresses.Add(adress2);
            Adresses.Add(adress3);

            Hospital hospital1 = new Hospital
            {
                ID = "1",
                Institution = "Hospital 1",
                Adress = adress1,
                Active = true
            };

            Hospital hospital2 = new Hospital
            {
                ID = "2",
                Institution = "Hospital 2",
                Adress = adress2,
                Active = true
            };

            Hospitals.Add(hospital1);
            Hospitals.Add(hospital2);

            RegisteredUser user1 = new RegisteredUser
            {
                ID = 1,
                Name = "Petar",
                Surname = "Petrovic",
                Username = "pero",
                Password = "123",
                Email = "petar@gmail.com",
                Adress = adress1,
                Active = true,
                JMBG = "1234567891234",
                Gender = EGender.MALE,
                UserType = EUserType.DOCTOR
            };

            RegisteredUser user2= new RegisteredUser
            {
                ID = 2,
                Name = "Darko",
                Surname = "Maric",
                Username = "darko",
                Password = "123",
                Email = "darko@gmail.com",
                Adress = adress2,
                Active = true,
                JMBG = "1234567891234",
                Gender = EGender.MALE,
                UserType = EUserType.PATIENT
            };
            Users.Add(user1);
            Users.Add(user2);
            

*//*            Doctor doctor = new Doctor
            {
                Hospital = "Hospital 3",
                User = user1
            };*//*

            Patient patinet1 = new Patient
            {
                User = user2,
                Therapies = new ObservableCollection<Therapy>(),
                Appointments = new ObservableCollection<Appointment>()
                
            };
            
            Appointment appointment = new Appointment
            {
                ID = "1",
                Doctor = doctor,
                Date = "datum",
                Status = EStatus.FREE,
                Patient = patinet1,
                Active = true
            };
            Appointment appointmen2 = new Appointment
            {
                ID = "2",
                Doctor = doctor,
                Date = "datums",
                Status = EStatus.FREE,
                Patient = patinet1,
                Active = true
            };
            Appointments.Add(appointment);
            Appointments.Add(appointmen2);

            Therapy therapy1 = new Therapy
            {
                ID = "1",
                Description = "Bolest 1",
                Doctor = doctor,
                Active = true
            };

            Therapy therapy2 = new Therapy
            {
                ID = "2",
                Description = "Bolest 2",
                Doctor = doctor,
                Active = true
            };

            Therapy therapy3 = new Therapy
            {
                ID = "3",
                Description = "Bolest3",
                Doctor = doctor,
                Active = true
            };
            Therapies.Add(therapy1);
            Therapies.Add(therapy2);
            Therapies.Add(therapy3);
            patinet1.Appointments.Add(appointmen2);
            patinet1.Therapies.Add(therapy2);
            Patients.Add(patinet1);

*/
        }

        public int SaveEntity(Object obj)
        {
            if (obj is RegisteredUser)
            {
                return _userService.saveEntity(obj);
            }
            else if (obj is Doctor)
            {
                return _doctorService.saveEntity(obj);
            }
            else if (obj is Patient)
            {
                return _patientService.saveEntity(obj);
            }
            else if (obj is Hospital)
            {
                return _hospitalService.saveEntity(obj);
            }
            else if (obj is Adress)
            {
                return _adressService.saveEntity(obj);
            }
            else if (obj is Therapy)
            {
                return _therapyService.saveEntity(obj);
            }
            else if (obj is Appointment)
            {
                return _appointmentService.saveEntity(obj);
            }

            return -1;
        }

        public void ReadEntity(string filename)
        {
            if (filename.Contains("users"))
            {
                _userService.readEntity();
            }
            else if (filename.Contains("doctors"))
            {
                _doctorService.readEntity();
            }
            else if (filename.Contains("patients"))
            {
                _patientService.readEntity();
            }
            else if (filename.Contains("adress"))
            {
                _adressService.readEntity();
            }
            else if (filename.Contains("hospital"))
            {
                _hospitalService.readEntity();
            }
            else if (filename.Contains("therapy"))
            {
                _therapyService.readEntity();
            }
            else if (filename.Contains("appointment"))
            {
                _appointmentService.readEntity();
            }
        }
        public void DeleteEntity(Object obj)
        {
            if (obj is Adress)
            { 
                _adressService.deleteEntity(obj);
            }
            else if (obj is Hospital)
            {
         
                _hospitalService.deleteEntity(obj);
            }
            else if (obj is RegisteredUser)
            {

                _userService.deleteEntity(obj);
            }
            else if (obj is Therapy)
            {

                _therapyService.deleteEntity(obj);
            }
            else if (obj is Appointment)
            {

                _appointmentService.deleteEntity(obj);
            }

        }

        public void UpdateEntity(Object obj)
        {
            if (obj is Adress)
            {
                _adressService.updateEntity(obj);
            }
            else if (obj is Hospital)
            {

                _hospitalService.updateEntity(obj);
            }
            else if (obj is RegisteredUser)
            {

                _userService.updateEntity(obj);
            }
            else if (obj is Therapy)
            {

                _therapyService.updateEntity(obj);
            }
            else if (obj is Appointment)
            {

                _appointmentService.updateEntity(obj);
            }

        }

        public Adress foundAdress(string ID)
        {
            int index = Util.Instance.Adresses.ToList().FindIndex(a => a.ID.Equals(ID));
            return Instance.Adresses[index];
        }

        public Patient foundPatient(string username)
        {
            foreach(Patient patient in Patients)
            {
                if (patient.User.Username.Equals(username))
                {
                    return patient;                    
                }
            }
            return null;
        }

        public Doctor findDoctor(string username)
        {
            foreach (Doctor doctor in Doctors)
            {
                if (doctor.User.Username.Equals(username))
                {
                    return doctor;
                }
            }
            return null;
        }

        public RegisteredUser foundUserById(int id)
        {
            foreach (RegisteredUser user in Users)
            {
                if (user.ID == id)
                {
                    return user;
                }
            }
            return null;
        }

        public Hospital foundHospitalById(int id)
        {
            foreach (Hospital hospital in Hospitals)
            {
                if (hospital.ID.Equals(id))
                {
                    return hospital;
                }
            }
            return null;
        }

        public ObservableCollection<Hospital>  findHospitalById(int id)
        {
            ObservableCollection<Hospital> doctor = new ObservableCollection<Hospital>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from hospital h join adress a on h.AdressId = a.id join Doctor d on d.HospitalId = h.Id where  h.id = @id";
                command.Parameters.Add(new SqlParameter("id", id));
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    doctor.Add(new Hospital
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Institution = reader.GetString(1),
                        Active = reader.GetBoolean(3),
                        Adress = new Adress
                        {
                            City = reader.GetString(7),
                            Number = reader.GetString(6),
                            State = reader.GetString(8),
                            Street = reader.GetString(5),
                            ID = reader.GetInt32(4).ToString(),
                            Active = reader.GetBoolean(9)
                        }
                    });

                }

                reader.Close();
            }
            return doctor;
        }
        public void listaDoktora()
        {
            foreach (Doctor doctor in Doctors)
            {
                Console.WriteLine(doctor.ToString());
            }
        }

        public Doctor findDoctorById(int id)
        {
            foreach(Doctor doctor in Doctors)
            {
                if(doctor.ID == id)
                {
                    return doctor;
                }
            }
            return null;
        }

        public Patient findPatientById(int id)
        {
            foreach (Patient patient in Patients)
            {
                if (patient.ID == id)
                {
                    return patient;
                }
            }
            return null;
        }

        public ObservableCollection<Appointment> findAppointmentsByUser(String username = null, Patient patient = null)
        {
            ObservableCollection<Appointment> termini = new ObservableCollection<Appointment>();
            if (username != null)
            {
                foreach (Appointment appointment in Appointments)
                {
                    if (appointment.Doctor.User.Username.Equals(username))
                    {
                        termini.Add(appointment);
                    }
                }
            }
            else if (patient != null)
            {
                foreach (Appointment appointment in Appointments)
                {
                    if (appointment.Patient != null && appointment.Patient.Equals(patient))
                    {
                        termini.Add(appointment);
                    }
                }
            }
            return termini;
        }

        public ObservableCollection<Therapy> findTherapiesByUser(Patient patient)
        {
            ObservableCollection<Therapy> terpaije = new ObservableCollection<Therapy>();
                foreach (Therapy therapy in Therapies)
                {
                    if (therapy.Patient.Equals(patient))
                    {
                        terpaije.Add(therapy);
                    }
                }
            return terpaije;
        }

        public int addFreeAppointment(object obj)
        {
            Appointment appointment = obj as Appointment;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into dbo.Appointment (DoctorId,  Date, Status, Active)
                                        output inserted.id VALUES (@DoctorId, @Date, @Status, @Active)";

                command.Parameters.Add(new SqlParameter("DoctorId", appointment.Doctor.ID));
                command.Parameters.Add(new SqlParameter("Date", appointment.Date));
                command.Parameters.Add(new SqlParameter("Status", appointment.Status));
                command.Parameters.Add(new SqlParameter("Active", appointment.Active));

                return (int)command.ExecuteScalar();
            }
        }

        public ObservableCollection<RegisteredUser> ShowLogged(RegisteredUser user)
        {

            ObservableCollection<RegisteredUser> lista = new ObservableCollection<RegisteredUser>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users u join adress a on u.AdressId = a.id
                                        where u.username = @username";
                command.Parameters.Add(new SqlParameter("username", user.Username));

                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Enum.TryParse(reader.GetString(7), out EGender gender);
                    Enum.TryParse(reader.GetString(8), out EUserType userType);
                    lista.Add(new RegisteredUser
                    {
                        ID = reader.GetInt32(0),
                        Username = reader.GetString(1),
                        Password = reader.GetString(2),
                        Name = reader.GetString(3),
                        Surname = reader.GetString(4),
                        Email = reader.GetString(5),
                        JMBG = reader.GetString(6),
                        Gender = gender,
                        UserType = userType,
                        Active = reader.GetBoolean(10),
                        Adress = new Adress
                        {
                            City = reader.GetString(14),
                            Number = reader.GetString(13),
                            State = reader.GetString(15),
                            Street = reader.GetString(12),
                            ID = reader.GetInt32(11).ToString(),
                            Active = reader.GetBoolean(16)
                        }
                    });

                }

                reader.Close();
            }
            return lista;
        }

        public ObservableCollection<RegisteredUser> findPatientsByDoctor(Doctor doctor)
        {

            ObservableCollection<RegisteredUser> lista = new ObservableCollection<RegisteredUser>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select DISTINCT u.*, a.* from users u join adress a on u.AdressId = a.id
                                        join Appointment ap on u.id = ap.PatientId where ap.DoctorId = @id";
                command.Parameters.Add(new SqlParameter("id", doctor.ID));

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Enum.TryParse(reader.GetString(7), out EGender gender);
                    Enum.TryParse(reader.GetString(8), out EUserType userType);
                    lista.Add(new RegisteredUser
                    {
                        ID = reader.GetInt32(0),
                        Username = reader.GetString(1),
                        Password = reader.GetString(2),
                        Name = reader.GetString(3),
                        Surname = reader.GetString(4),
                        Email = reader.GetString(5),
                        JMBG = reader.GetString(6),
                        Gender = gender,
                        UserType = userType,
                        Active = reader.GetBoolean(10),
                        Adress = new Adress
                        {
                            City = reader.GetString(14),
                            Number = reader.GetString(13),
                            State = reader.GetString(15),
                            Street = reader.GetString(12),
                            ID = reader.GetInt32(11).ToString(),
                            Active = reader.GetBoolean(16)
                        }
                    });

                }

                reader.Close();
            }
            return lista;
        }



        public ObservableCollection<Hospital> SearchHospitalByDoctor(Doctor doctor)
        {
            ObservableCollection<Hospital> lista = new ObservableCollection<Hospital>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from hospital h join adress a on h.AdressId = a.id join Doctor d on d.HospitalId = h.id where d.id = @id";
                command.Parameters.Add(new SqlParameter("id", doctor.ID));
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    lista.Add(new Hospital
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Institution = reader.GetString(1),
                        Active = reader.GetBoolean(3),
                        Adress = new Adress
                        {
                            City = reader.GetString(7),
                            Number = reader.GetString(6),
                            State = reader.GetString(8),
                            Street = reader.GetString(5),
                            ID = reader.GetInt32(4).ToString(),
                            Active = reader.GetBoolean(9)
                        }
                    });

                }

                reader.Close();
            }
            return lista;
        }

        public ObservableCollection<Hospital> SearchHospitalByAdress(Adress adress)
        {
            ObservableCollection<Hospital> lista = new ObservableCollection<Hospital>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from hospital h join adress a on h.AdressId = a.id where h.AdressId = @id";
                command.Parameters.Add(new SqlParameter("id", adress.ID));
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    lista.Add(new Hospital
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Institution = reader.GetString(1),
                        Active = reader.GetBoolean(3),
                        Adress = new Adress
                        {
                            City = reader.GetString(7),
                            Number = reader.GetString(6),
                            State = reader.GetString(8),
                            Street = reader.GetString(5),
                            ID = reader.GetInt32(4).ToString(),
                            Active = reader.GetBoolean(9)
                        }
                    });

                }

                reader.Close();
            }
            return lista;
        }

        public ObservableCollection<Therapy> findTherapByPatient(Patient patient)
        {
            ObservableCollection<Therapy> lista = new ObservableCollection<Therapy>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from Therapy where PatientId = @id";
                command.Parameters.Add(new SqlParameter("id", patient.ID));
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    lista.Add(new Therapy
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Description = reader.GetString(1),
                        Doctor = Util.Instance.findDoctorById(reader.GetInt32(2)),
                        Patient = Util.Instance.findPatientById(reader.GetInt32(3)),
                        Active = reader.GetBoolean(4)
                    });

                }

                reader.Close();
            }
            return lista;
        }

        public ObservableCollection<Appointment> findAppointmentByDoctor(Doctor doctor)
        {
            ObservableCollection<Appointment> lista = new ObservableCollection<Appointment>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from appointment a left join Doctor d on a.DoctorId = d.id left join patient p  on a.PatientId = p.id where a.DoctorId = @id";
                command.Parameters.Add(new SqlParameter("id", doctor.ID));
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {
                    Patient patient;
                    if (reader.IsDBNull(2))
                    {
                        patient = null;
                    }
                    else
                    {
                        patient = Util.Instance.findPatientById(reader.GetInt32(2));
                    }
                    Enum.TryParse(reader.GetString(4), out EStatus estatus);

                    lista.Add(new Appointment
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Doctor = Util.Instance.findDoctorById(reader.GetInt32(1)),
                        Patient = patient,
                        Date = reader.GetDateTime(3),
                        Status = estatus,
                        Active = reader.GetBoolean(5)
                    });

                }

                reader.Close();
            }
            return lista;
        }

        public ObservableCollection<Appointment> findAppointmentByPatient(Patient patient)
        {
            ObservableCollection<Appointment> lista = new ObservableCollection<Appointment>();


            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from appointment a left join Doctor d on a.DoctorId = d.id left join patient p  on a.PatientId = p.id where a.PatientId = @id";
                command.Parameters.Add(new SqlParameter("id", patient.ID));
                SqlDataReader reader = command.ExecuteReader();


                while (reader.Read())
                {

                    Enum.TryParse(reader.GetString(4), out EStatus estatus);

                    lista.Add(new Appointment
                    {
                        ID = reader.GetInt32(0).ToString(),
                        Doctor = Util.Instance.findDoctorById(reader.GetInt32(1)),
                        Patient = patient,
                        Date = reader.GetDateTime(3),
                        Status = estatus,
                        Active = reader.GetBoolean(5)
                    });

                }

                reader.Close();
            }
            return lista;
        }




    }
}
