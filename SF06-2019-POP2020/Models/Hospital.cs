﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Models
{
    public class Hospital
    {
        /* šifra(jedinstvena), naziv institucije i Adresa u kom se nalazi*/

        private string _id;

        public  string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _institution;

        public string Institution
        {
            get { return _institution; }
            set { _institution = value; }
        }

        private Adress _adress;

        public  Adress Adress
        {
            get { return _adress; }
            set { _adress = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public override bool Equals(object obj)
        {
            var item = obj as Hospital;

            if (item == null)
            {
                return false;
            }

            return this.ID.Equals(item.ID);
        }


        public Hospital Clone()
        {
            Hospital copy = new Hospital();

            copy.ID = ID;
            copy.Institution = Institution;
            copy.Adress = Adress;
            copy.Active = Active;

            return copy;
        }

        public override string ToString()
        {
            return "Institution: " + Institution;
        }


    }
}
