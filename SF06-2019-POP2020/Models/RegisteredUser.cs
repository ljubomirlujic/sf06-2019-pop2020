﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Models
{   
    [Serializable]
    public class RegisteredUser : IDataErrorInfo
    {

        /* ime, prezime, JMBG(jedinstveno), email, Adresa(adresa stanovanja),
             pol, lozinka, tip registrovanog korisnika(registrovani korisnici 
                 aplikacije su: administratori, lekari i pacijenti). 
 */

        public RegisteredUser()
        {

        }
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private String _username;

        public String Username
        {
            get { return _username; }
            set { _username = value; }
        }
        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }

        }

        private string _surname;

        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        private string _jmbg;

        public string JMBG
        {
            get { return _jmbg; }
            set { _jmbg = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private Adress _adress;

        public Adress Adress
        {
            get { return _adress; }
            set { _adress = value; }
        }

        private EGender _gender;

        public EGender Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }


        private EUserType _userType;

        public  EUserType UserType
        {
            get { return _userType; }
            set { _userType = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public string Error
        {
            get
            {
                return "message!";
            }
        }

        public string this[string columnName]
        {

            get
            {

                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(Name))
                            return "You must enter a value. Name is mandatory";
                        break;
                    case "Surname":
                        if (string.IsNullOrEmpty(Surname))
                            return "You must enter a value. Surname is mandatory";
                        break;
                    case "Username":
                        if (string.IsNullOrEmpty(Username) || Util.Instance.Users.ToList().Exists(k => k.Username.Equals(Username)))
                            return "Username is mandatory. Make sure it's unique";
                        break;
                    case "Email":
                        if (string.IsNullOrEmpty(Email) || !Email.ToString().Contains("@") || !Email.ToString().EndsWith(".com"))
                        {
                            return "Invalid email format";
                        }
                        break;
                    case "JMBG":
                        if (string.IsNullOrEmpty(JMBG) || JMBG.ToString().Length != 13 || (int.TryParse(JMBG.ToString(), out int value)))
                            return "JMBG must consist of 13 numbers";
                        break;
                    case "Password":
                        if (string.IsNullOrEmpty(Password))
                            return "Password is mandatory";
                        break;
                }

                return string.Empty;
            }

        }

        public override string ToString()
        {
            return "User: " + Username + ", Name and Surname:" + Name + " " + Surname + 
                ", Email Adress: " + Email + /*", Users adress: " + Adress +*/ ", Type of user: " + UserType;
        }


     /*   ovde ce trebati da se doda jos adresa*/
        public string forWriter()
        {
            return Username + ";" + Name + ";" + Surname + ";" + JMBG +";" + Email + ";" + Password + ";" + Gender + ";" +  UserType + ";" + Adress.ID + ";" + Active;
        }


        public RegisteredUser Clone()
        {
            RegisteredUser copy = new RegisteredUser();

            copy.Adress = Adress;
            copy.Active = Active;
            copy.Email = Email;
            copy.Name = Name;
            copy.Surname = Surname;
            copy.Gender = Gender;
            copy.Password = Password;
            copy.JMBG = JMBG;
            copy.Username = Username;
            copy.UserType = UserType;

            return copy;
        }










    }
}
