﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Models
{
    [Serializable]
    public class Doctor
    {
        /*  Dom zdravlja u kojem je lekar zaposlen, i lista Termina(zakazanih i/ili slobodnih)*/
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private RegisteredUser _user;

        public RegisteredUser User
        {
            get { return _user; }
            set { _user = value; }
        }


        private Hospital _hospital;

        public Hospital Hospital
        {
            get { return _hospital; }
            set { _hospital = value; }
        }

        private ObservableCollection<Appointment> _appointments;

        public ObservableCollection<Appointment> Appointments
        {
            get { return _appointments; }
            set { _appointments = value; }
        }

        public override string ToString()
        {
            return User.Username;
        }


    }
}

