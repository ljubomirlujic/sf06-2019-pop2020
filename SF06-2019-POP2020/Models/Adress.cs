﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Models
{
    [Serializable]
    public class Adress
    {
        /* id(jedinstven), ulica, broj, grad, država*/

        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _street;

        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }

        private string _number;

        public string Number
        {
            get { return _number; }
            set { _number = value; }
        }

        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public override bool Equals(object obj)
        {
            var item = obj as Adress;

            if (item == null)
            {
                return false;
            }

            return this.ID.Equals(item.ID);
        }

        public override string ToString()
        {
            return "Street: " + Street + " , Street number: " + Number + " City: " + City;
        }

        public Adress Clone()
        {
            Adress copy = new Adress();

            copy.ID = ID;
            copy.Street = Street;
            copy.Number = Number;
            copy.City = City;
            copy.State = State;
            copy.Active = Active;

            return copy;
            
        }



    }
}
