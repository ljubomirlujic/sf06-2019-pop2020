﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Models
{
    public class Therapy
    {
        /*šifra(jedinstvena), opis terapije, Lekar koji je prepisao terapiju.*/


        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private Doctor _doctor;

        public Doctor Doctor
        {
            get { return _doctor; }
            set { _doctor = value; }
        }
        private Patient _patient;

        public Patient Patient
        {
            get { return _patient; }
            set { _patient = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public Therapy Clone()
        {
            Therapy copy = new Therapy();

            copy.ID = ID;
            copy.Doctor = Doctor;
            copy.Patient = Patient;
            copy.Description = Description;
            copy.Active = Active;

            return copy;

        }





    }
}
