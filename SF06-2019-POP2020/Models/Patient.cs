﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Models
{
    public class Patient
    {

        /*Registrovan korisnik(korisničko ime), lista zakazanih Termina i lista Terapija*/
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        private RegisteredUser registeredUser;

        public RegisteredUser User
        {
            get { return registeredUser; }
            set { registeredUser = value; }
        }


        private  ObservableCollection<Appointment> _appointments;

        public ObservableCollection<Appointment> Appointments 
        {
            get { return _appointments; }
            set { _appointments = value; }
        }

        private ObservableCollection<Therapy> _therapies;

        public ObservableCollection<Therapy> Therapies
        {
            get { return _therapies; }
            set { _therapies = value; }
        }

        public override string ToString()
        {
            return User.Username;
        }

        public string forWriter()
        {
            return User.Username;
        }



    }
}
