﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF06_2019_POP2020.Models
{
    public class Appointment
    {

        /*  šifra(jedinstvena), Lekar kod kojeg je termin zakazan, datum termina,
              status termina(slobodan ili zakazan) i Pacijent koji je zakazao termin.
              Napomena: Termin postoji iako je u status SLOBODAN.*/

        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private Doctor _doctor;

        public Doctor Doctor
        {
            get { return _doctor; }
            set { _doctor = value; }
        }

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private Patient _patient;

        public Patient Patient
        {
            get { return _patient; }
            set { _patient = value; }
        }

        private EStatus _status;

        public EStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private bool _active;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public override string ToString()
        {
            return "Appointment date: " + Date + ", Patient: " + Patient + ", Doctor: " + Doctor + Status;
        }

        public Appointment Clone()
        {
            Appointment copy = new Appointment();

            copy.ID = ID;
            copy.Doctor = Doctor;
            copy.Patient = Patient;
            copy.Date = Date;
            copy.Status = Status;
            copy.Active = Active;

            return copy;

        }







    }
}
