﻿using SF06_2019_POP2020.Models;
using SF06_2019_POP2020.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace SF06_2019_POP2020
{
    /// <summary>
    /// Interaction logic for HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : Window
    {
        RegisteredUser CurrentUser;
        public HomeWindow(RegisteredUser user = null)
        {
            InitializeComponent();

            Util.Instance.ReadEntity("adress");
            Util.Instance.ReadEntity("hospital");
            Util.Instance.ReadEntity("users");
            Util.Instance.ReadEntity("doctors");
            Util.Instance.ReadEntity("patients");
            Util.Instance.ReadEntity("therapy");
            Util.Instance.ReadEntity("appointment");

            CurrentUser = user;
            if(user == null)
            {
                MIAdress.Visibility = Visibility.Collapsed;
                MIPatients.Visibility = Visibility.Collapsed;
                MIDoctors.Visibility = Visibility.Collapsed;
                MIAppointment.Visibility = Visibility.Collapsed;
                MITherapy.Visibility = Visibility.Collapsed;

            }
            else if (user.UserType.Equals(EUserType.PATIENT))
            {
                MIAdress.Visibility = Visibility.Collapsed;
                MIPatients.Header = "Profile";
                MIRegistration.Visibility = Visibility.Collapsed;
            }
            else if (user.UserType.Equals(EUserType.DOCTOR))
            {
                MIAdress.Visibility = Visibility.Collapsed;
                MITherapy.Visibility = Visibility.Collapsed;
                MIDoctors.Header = "Profile";
                MIRegistration.Visibility = Visibility.Collapsed;
            }
            else
            {

            }
        }

        private void MIDoctors_Click(object sender, RoutedEventArgs e)
        {
            AllDoctors doctorWindow = new AllDoctors(CurrentUser);

            this.Hide();
            doctorWindow.Show();
        }
        private void MIPatients_Click(object sender, RoutedEventArgs e)
        {
            AllPatients patinetWindow = new AllPatients(CurrentUser);

            this.Hide();
            patinetWindow.Show();

            
        }
        private void MIAdress_Click(object sender, RoutedEventArgs e)
        {
            AllAdress adressWindow = new AllAdress(CurrentUser);

            this.Hide();
            adressWindow.Show();
        }

        private void MIHospital_Click(object sender, RoutedEventArgs e)
        {
            AllHospitals hospitalWindow = new AllHospitals(CurrentUser);

            this.Hide();
            hospitalWindow.Show();
        }

        private void MIAppointment_Click(object sender, RoutedEventArgs e)
        {
            AllAppointments appointmentWindow = new AllAppointments(CurrentUser);

            this.Hide();
            appointmentWindow.Show();
        }

        private void MITherapy_Click(object sender, RoutedEventArgs e)
        {
            AllTherapy therapyWindow = new AllTherapy(CurrentUser);

            this.Hide();
            therapyWindow.Show();
        }

        private void MIRegistration_Click(object sender, RoutedEventArgs e)
        {
             RegisteredUser newUser = new RegisteredUser();
            Registration rw = new Registration(newUser);
            this.Hide();
            if (!(bool)rw.ShowDialog())
            {
                this.Show();
            }
            else
            {
                LoginWindow lw = new LoginWindow();
                lw.Show();
            }
        }
    }
}
